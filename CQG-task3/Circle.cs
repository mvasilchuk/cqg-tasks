﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQG_task3
{
    class Circle : Shape
    {
        bool PointInside(Point point)
        {
            return false;
        }

        override double Perimeter(double r){
            double PI = 3.14;
            double perimeter = 2*PI*r;

            return perimeter;
        }

        override double Area(double r)
        {
            double PI = 3.14;
            double area = PI*r*r;

            return area;
        }
    }
}
