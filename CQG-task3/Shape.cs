﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQG_task3
{
    abstract class Shape
    {
        abstract double Area();
        abstract double Perimeter();
    }
}
