﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQG_task3
{
    class Rectangle
    {
        Point a, b, c, d;
        public Rectangle(Point[] args)
        {
            this.a = args[0];
            this.b = args[1];
            this.c = args[2];
            this.d = args[3];
        }
        bool PointInside(Point point)
        {
            if (point.Y < a.Y && point.Y < b.Y && point.Y > c.Y && point.Y > d.Y)
            {
                if (point.X < b.X && point.X < c.X && point.X > a.X && point.X > d.X)
                {
                    return true;
                }
               
            }
            
            return false;
        }

        override double Perimeter(double a, double b)
        {
            return 2 * (a + b);
        }

        override double Area(double a, double b)
        {
            return a*b;
        }
    }
}
