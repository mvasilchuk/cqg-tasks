﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp = null;
            Console.WriteLine("1- Create employee; 2 - see employee information");
            string choice = Console.ReadLine();

            
            if (choice == "1")
            {
                emp = new Employee("Lex", "QA", 1000d);
            }
            else if (choice == "2")
            {
                emp.printEmployee();
            }
        }
    }
}
