﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Employee
    {
        string name, position;
        double salary;

        public Employee(string name, string position, double salary)
        {
            this.name = name;
            this.position = position;
            this.salary = salary;

        }

        public void printEmployee()
        {
            Console.WriteLine("\nName: "+name);
            Console.WriteLine("\nPosition: " + position);
            Console.WriteLine("\nSalary: " + salary);
        }
    }
}
